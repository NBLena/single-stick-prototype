@tool

extends Area2D
@export var ENTITIES: Array[NodePath] = []
@export var DELETE: Array[NodePath] = []

func _ready():
	if Engine.is_editor_hint():
		var sprite = Sprite2D.new()
		sprite.texture = load("res://Assets/Editor/exit.png")
		sprite.position.y -= 10
		add_child(sprite)
		
func _process(_delta):
	# keep collision shape centered
	$CollisionShape2D.position = Vector2(0,0)

func _on_body_entered(body):
	if body.is_in_group("Player"):
		# Remove covers first, so enemies that are activated at the same time, dont get pushed out by the collision of the cover
		for toDelete in DELETE:
			var node = get_node_or_null(toDelete)
			if node == null:
				push_error("The given node path for cover " + toDelete.get_name(0) + " was not found")
				return
			node.queue_free()
		for entity in ENTITIES:
			var node = get_node_or_null(entity)
			if node == null:
				return
			if node.get("active") != null:
				get_node(entity).active = true
			else:
				push_error("The given entity " + node.name + " can not be set active by this trigger")
		
		# We are done!
		queue_free()
