extends Node2D

@export var INTERVAL = 2.0
@export var ACTIVE = true
@export var EnemyResourcePath = "res://Scenes/Entities/Enemy/GenericEnemy.tscn"

@onready var _timer = $Timer

var active = false

# Called when the node enters the scene tree for the first time.
func _ready():
	active = ACTIVE
	_timer.start(INTERVAL)

func _on_timer_timeout():
	if not active:
		return
	var new_enemy = load(EnemyResourcePath).instantiate()
	new_enemy.transform = transform
	get_parent().add_child(new_enemy)
	_timer.start(INTERVAL)
