extends CharacterBody2D

var HitsPlayer = false
var HitsEnemies = true
var DAMAGE = 0.0
var ProjectileType: GlobalConstants.ProjectileType = GlobalConstants.ProjectileType.BREAKABLE

@onready var _sprite = $Sprite2D
@onready var _projectileDamageArea = $ProjectileDamageArea

func _ready():
	set_motion_mode(MOTION_MODE_FLOATING) # all collision is reported as on_wall()
	if HitsPlayer:
		_projectileDamageArea.add_to_group("Damage")
		_projectileDamageArea.add_to_group("Projectile")

	if (ProjectileType == GlobalConstants.ProjectileType.BLOCKABLE):
		_sprite.modulate = Color(1, 0, 0)
	if (ProjectileType == GlobalConstants.ProjectileType.UNBLOCKABLE):
		_sprite.modulate = Color(0.2, 0.2, 0.2)

func _physics_process(_delta):
	if move_and_slide():
		# the arrow collided with something!
		queue_free()

func _on_crossbow_damage_area_area_entered(area):
	if HitsEnemies and area.is_in_group("Enemy"):
		area.get_parent().damage(DAMAGE)
	if HitsEnemies and area.is_in_group("Projectile"):
		if area.get_parent().ProjectileType == GlobalConstants.ProjectileType.BREAKABLE:
			area.get_parent().queue_free()

func _on_visible_on_screen_notifier_2d_screen_exited():
	queue_free()
