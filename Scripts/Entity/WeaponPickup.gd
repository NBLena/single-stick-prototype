extends Node2D

@export_file var weapon_scene
var weapon

func _ready():
	weapon = load(weapon_scene).instantiate()

func _on_pickup_area_body_entered(body):
	if body.is_in_group("Player"):
		body.add_weapon(weapon)
		queue_free()
