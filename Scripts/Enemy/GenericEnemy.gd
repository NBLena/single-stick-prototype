extends CharacterBody2D

@export var MAX_HEALTH = 1
@export var SPEED = 100
@export var DAMAGE = 1
@export var HIT_I_FRAMES_SECONDS = 3.0
@export var DEATH_ANIMATION_SECONDS = 0.3
enum ACTIVE_STATES {INACTIVE, ACTIVE, ACTIVE_WHEN_ON_SCREEN, ACTIVE_ONLY_WHEN_ON_SCREEN}
@export var ACTIVE: ACTIVE_STATES = ACTIVE_STATES.ACTIVE

@onready var _sprite = $Sprite
@onready var _deathTimer = $DeathTimer
@onready var _visibleOnScreenNotifier = $VisibleOnScreenNotifier2D
@onready var _enemyHintbox = $EnemyHitbox
@onready var _enemyCollision = $EnemyCollision
@onready var _raycast = $RayCast2D
@onready var _iFrameTimer = $"I-Frame Timer"

const DeathColorStart = Color(0,1,1,1)
const DeathColorEnd = Color(0.5,0.5,0.5,0)

var mobile = true
var health = null
var dying = false
var active = false
var spriteScaleAtTimeOfDeath = null
var spriteScaleFactorAtDeath = 1.5
var deathAnimationFirstPart = 0.8

# Called when the node enters the scene tree for the first time.
func _ready():
	health = MAX_HEALTH
	set_motion_mode(MOTION_MODE_FLOATING) # all collision is reported as on_wall()
	active = ACTIVE == ACTIVE_STATES.ACTIVE
	if ACTIVE == ACTIVE_STATES.ACTIVE_WHEN_ON_SCREEN or ACTIVE == ACTIVE_STATES.ACTIVE_ONLY_WHEN_ON_SCREEN:
		_visibleOnScreenNotifier.visible = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if dying:
		_sprite.modulate = DeathColorEnd.lerp(DeathColorStart, _deathTimer.get_time_left() / DEATH_ANIMATION_SECONDS)
		if _deathTimer.get_time_left() > (DEATH_ANIMATION_SECONDS * deathAnimationFirstPart):
			# first half of death animation
			_sprite.scale = Vector2(spriteScaleAtTimeOfDeath.x, lerp(spriteScaleAtTimeOfDeath.y * spriteScaleFactorAtDeath, spriteScaleAtTimeOfDeath.y, (_deathTimer.get_time_left() - (DEATH_ANIMATION_SECONDS * deathAnimationFirstPart)) / (DEATH_ANIMATION_SECONDS * deathAnimationFirstPart)))
		else:
			# second half of death animation
			_sprite.material = null # TODO(Lena): This doesnt have to happen every frame in the second half of the animation, only once
			_sprite.scale = Vector2(spriteScaleAtTimeOfDeath.x, lerp(spriteScaleAtTimeOfDeath.y, spriteScaleAtTimeOfDeath.y * spriteScaleFactorAtDeath, (_deathTimer.get_time_left() / (DEATH_ANIMATION_SECONDS * deathAnimationFirstPart))))
		return
	
	var player_ref = get_tree().get_first_node_in_group("Player")

	transform = transform.looking_at(player_ref.transform.origin)
	#TODO (Lena): this is pretty stupid, find a better way to do this
	_sprite.rotation = rotation
	_enemyCollision.rotation = rotation
	_enemyHintbox.rotation = rotation
	
	velocity = Vector2.RIGHT.rotated(rotation) * SPEED
	
	if mobile and active:
		move_and_slide()
	
	rotation = 0.0
	
func _physics_process(_delta):
	_raycast.target_position = get_tree().get_first_node_in_group("Player").global_position - global_position

func damage(damageAmount):
	if _iFrameTimer.is_stopped():
		health -= damageAmount
		if (health <= 0):
			die()
			return
		_sprite.modulate.a = 0.5
		if HIT_I_FRAMES_SECONDS < 0.01:
			_iFrameTimer.start(0.01)
		else:
			_iFrameTimer.start(HIT_I_FRAMES_SECONDS)

func damageIfHit(damageAmount):
	# Is the line between the player and this enemy unobstructed?
	if not _raycast.is_colliding():
		damage(damageAmount)

func die():
	remove_child(_enemyHintbox)
	remove_child(_enemyCollision)
	_deathTimer.start(DEATH_ANIMATION_SECONDS)
	_sprite.material = load("res://Shaders/Hit_Shader_Material.tres")
	spriteScaleAtTimeOfDeath = _sprite.transform.get_scale()
	dying = true

func _on_i_frame_timer_timeout():
	_sprite.modulate.a = 1

func _on_death_timer_timeout():
	queue_free()

func _on_visible_on_screen_notifier_2d_screen_entered():
	active = true

func _on_visible_on_screen_notifier_2d_screen_exited():
	if ACTIVE == ACTIVE_STATES.ACTIVE_ONLY_WHEN_ON_SCREEN:
		active = false
