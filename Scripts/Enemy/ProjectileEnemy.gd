extends "res://Scripts/Enemy/GenericEnemy.gd"

@export var PROJECTILE_INTERVAL = 1.0
@export var PROJECTILE_SPEED = 500.0
@export var PROJECTILE_TYPE: GlobalConstants.ProjectileType

@onready var _projectileTimer = $ProjectileTimer

const projectile = preload("res://Scenes/Entities/Enemy/enemy_projectile.tscn")

func _ready():
	super()
	mobile = false
	_projectileTimer.start(PROJECTILE_INTERVAL)


func _on_projectile_timer_timeout():
	if not active:
		return

	var new_projectile = projectile.instantiate()
	new_projectile.HitsPlayer = true
	new_projectile.HitsEnemies = false
	new_projectile.DAMAGE = DAMAGE
	new_projectile.ProjectileType = PROJECTILE_TYPE
	
	var player_ref = get_tree().get_first_node_in_group("Player")
	rotation = transform.looking_at(player_ref.transform.origin).get_rotation()
	new_projectile.velocity = Vector2.from_angle(rotation) * PROJECTILE_SPEED
	
	add_child(new_projectile)
	_projectileTimer.start()


func _on_visible_on_screen_notifier_2d_screen_entered():
	if ACTIVE == ACTIVE_STATES.ACTIVE_WHEN_ON_SCREEN or ACTIVE == ACTIVE_STATES.ACTIVE_ONLY_WHEN_ON_SCREEN:
		active = true


func _on_visible_on_screen_notifier_2d_screen_exited():
	if ACTIVE == ACTIVE_STATES.ACTIVE_ONLY_WHEN_ON_SCREEN:
		active = false
