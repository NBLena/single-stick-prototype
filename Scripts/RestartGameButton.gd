extends Button

func _ready():
	grab_focus()
	
func _input(event):
	if get_parent().get_parent().get_parent().visible && event.is_action_released("ui_accept"):
		restart_game()

func _on_pressed():
	restart_game()

func restart_game():
	get_tree().reload_current_scene()
