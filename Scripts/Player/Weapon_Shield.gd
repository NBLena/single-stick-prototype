extends Node2D

@export var DAMAGE = 0.5

func _process(_delta):
	transform = transform.looking_at(get_parent().velocity)

func _on_area_2d_area_entered(area):
		if area.is_in_group("Enemy"):
			area.get_parent().damageIfHit(DAMAGE)
		if area.is_in_group("Projectile"):
			if area.get_parent().ProjectileType == GlobalConstants.ProjectileType.BLOCKABLE:
				area.get_parent().queue_free()
