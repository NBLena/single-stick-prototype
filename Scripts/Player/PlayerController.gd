extends CharacterBody2D

@export_group("Starting Weapons")
@export_file var START_WEAPON_1 = null
@export_file var START_WEAPON_2 = null
@export_group("Stats")
@export var SPEED = 500.0
@export var MAX_HEALTH = 4
@export var HIT_I_FRAMES_SECONDS = 2.0
@export_group("Dash Properties")
@export var DASH_SPEED = 1500.0
@export var DASH_SECONDS = 0.2
@export var DASH_STUN_SECONDS = 1.0
@export var DASH_FLICK_TIMEFRAME = 0.25
@export var DASH_FLICK_ANGLE = 10.0
@export_group("Cheats")
@export var CHEAT_GOD_MODE = false

@onready var _sprite = $Sprite
@onready var _stunnedSprite = $StunnedSprite
@onready var _hitbox = $Hitbox
@onready var _iFrameTimer = $"I-Frame Timer"
@onready var _dashWindowTimer = $"Dash-Window-Timer"
@onready var _dashMoveTimer = $"Dash-Move-Timer"
@onready var _gameOverUI = $GameOverUI
@onready var _healthUI = $HealthUI

var weapon_1 = null
var weapon_2 = null

var health = MAX_HEALTH
var dead = false
var walk_direction = "right"
var step_right = false
var step_offset = 0
var sprite_scale_x_at_ready

var dashing = false
var start_moving_angle = null
var stunned = false

func _ready():
	set_motion_mode(MOTION_MODE_FLOATING) # all collision is reported as on_wall()
	if START_WEAPON_1:
		weapon_1 = load(START_WEAPON_1).instantiate()
		add_child(weapon_1)
	if START_WEAPON_2:
		weapon_2 = load(START_WEAPON_2).instantiate()
		add_child(weapon_2)
	
	health = MAX_HEALTH
	update_health_ui()
	sprite_scale_x_at_ready = _sprite.scale.x
	
func _process(_delta):
	if dead:
		return
	if _hitbox.has_overlapping_areas():
		for area in _hitbox.get_overlapping_areas():
			if _iFrameTimer.is_stopped() and (area.is_in_group("Enemy") or area.is_in_group("Damage")):
				damage(area.get_parent().DAMAGE)
				
				
func damage(damageAmount: int):
	if dashing or CHEAT_GOD_MODE:
		return
	health -= damageAmount
	update_health_ui()
	if (health <= 0):
		game_over()
		return
	_sprite.modulate.a = 0.5
	_iFrameTimer.start(HIT_I_FRAMES_SECONDS)
		
func add_weapon(weapon):
	if weapon_2:
		remove_child(weapon_2)
	weapon_2 = weapon_1
	weapon_1 = weapon
	call_deferred("add_child", weapon_1)
	
func dash():
	_dashWindowTimer.stop()
	_dashMoveTimer.start(DASH_SECONDS)
	_iFrameTimer.start(DASH_SECONDS)
	_sprite.modulate.a = 0.5
	set_collision_layer_to_bypass_obstacles(true)
	dashing = true

func stop_dash_and_start_stun():
	dashing = false
	stunned = true
	_dashMoveTimer.start(DASH_STUN_SECONDS)
	velocity = Vector2(0.0,0.0)
	_stunnedSprite.modulate.r = 0.5
	_stunnedSprite.modulate.b = 0.5
	_sprite.visible = false
	_stunnedSprite.visible = true
	set_collision_layer_to_bypass_obstacles(false)

func stop_stun():
	stunned = false
	_stunnedSprite.modulate.r = 1.0
	_stunnedSprite.modulate.b = 1.0
	_stunnedSprite.visible = false
	_sprite.visible = true

func set_collision_layer_to_bypass_obstacles(activate: bool):
	set_collision_mask_value(3 if activate else 2, true)
	set_collision_mask_value(2 if activate else 3, false)

# HELPER FUNCTIONS FOR INPUT:
func is_player_stopped():
	return velocity.x == 0 and velocity.y == 0
func has_player_just_started_moving(xDirection, yDirection):
	return (velocity.x == 0 and velocity.y == 0) and (xDirection > 0 or xDirection < 0 or yDirection > 0 or yDirection < 0)
	
func check_input():
	if Input.is_action_pressed("QuitGame"):
		get_tree().change_scene_to_file("res://Scenes/Menus/MainMenu.tscn")
	
	# Get the input direction and handle the movement/deceleration.
	var xDirection = Input.get_axis("Move_Left", "Move_Right")

	var yDirection = Input.get_axis("Move_Up", "Move_Down")
	
	# Check if the stick was flicked
	if _dashWindowTimer.is_stopped():
		if has_player_just_started_moving(xDirection, yDirection):
			_dashWindowTimer.start(DASH_FLICK_TIMEFRAME)
			start_moving_angle = Vector2(xDirection, yDirection).normalized()
	else:
		var currentDirection = Vector2(xDirection, yDirection).normalized()
		var angleDifference = abs(rad_to_deg(currentDirection.angle() - start_moving_angle.angle()))
		if xDirection == 0 and yDirection == 0:
			angleDifference = 0
		elif angleDifference > 180:
			angleDifference = 360.0 - angleDifference
		angleDifference = snappedf(angleDifference, 0.1)
		
		if UserOptions.dashByDoubleTap and has_player_just_started_moving(xDirection, yDirection) and angleDifference < DASH_FLICK_ANGLE:
			dash()
		elif UserOptions.dashByFlick and angleDifference > 180.0 - DASH_FLICK_ANGLE:
			dash()
		elif UserOptions.dashByButton and Input.is_action_just_pressed("ui_accept"): #TODO: This doesnt work yet?
			dash()

	if xDirection:
		velocity.x = xDirection
	else:
		velocity.x = 0

	if yDirection:
		velocity.y = yDirection
	else:
		velocity.y = 0
	
func _physics_process(_delta):
	if dead:
		return 
	
	if not dashing and not stunned:
		check_input()
	
	velocity = velocity.normalized()
	if dashing:
		velocity *= DASH_SPEED
	else:	
		velocity *= SPEED
	
	velocity = velocity.round()

	move_and_slide()
	
	# set walk direction
	if velocity.y > 0:
		walk_direction = "down"
	elif velocity.y < 0:
		walk_direction = "up"
		
		
	if velocity.x > 0:
		walk_direction = "right"
		_sprite.scale.x = sprite_scale_x_at_ready
	elif velocity.x < 0:
		walk_direction = "left"
		_sprite.scale.x = -1 * sprite_scale_x_at_ready
		
	if walk_direction == "down":
		_sprite.frame = 4 + step_offset
	if walk_direction == "up":
		_sprite.frame = 1 + step_offset
	if walk_direction == "left" or walk_direction == "right":
		_sprite.frame = 7 + step_offset

func game_over():
	if dead:
		return
	remove_child(_hitbox)
	remove_child(_sprite)
	if weapon_1:
		remove_child(weapon_1)
	if weapon_2:
		remove_child(weapon_2)
	$"Walk-Animation-Timer".stop()
	SPEED = 0
	dead = true
	_gameOverUI.visible = true
	
func update_health_ui():
	if health > 0:
		_healthUI.get_node("Heart1").visible = true
	else:
		_healthUI.get_node("Heart1").visible = false
		
	if health > 1:
		_healthUI.get_node("Heart2").visible = true
	else:
		_healthUI.get_node("Heart2").visible = false
		
	if health > 2:
		_healthUI.get_node("Heart3").visible = true
	else:
		_healthUI.get_node("Heart3").visible = false
		
	if health > 3:
		_healthUI.get_node("Plus").visible = true
	else:
		_healthUI.get_node("Plus").visible = false

func _on_i_frame_timer_timeout():
	_sprite.modulate.a = 1

func _on_walk_animation_timer_timeout():
	if velocity.x == 0 and velocity.y == 0:
		step_right = false
		step_offset = 0
		return
	
	if _sprite.frame == 4 or _sprite.frame == 1 or _sprite.frame == 7:
		if step_right:
			step_offset = 1
		else:
			step_offset = -1
	else:
		step_offset = 0
		step_right = not step_right


func _on_dash_move_timer_timeout():
	if dead:
		return
	if dashing:
		stop_dash_and_start_stun()
	else:
		stop_stun()
	
