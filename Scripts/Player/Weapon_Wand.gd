extends Node2D
class_name Weapon_Wand

@export var INTERVAL = 1.0
@export var DAMAGE = 1.0
@export var PROJECTILE_SPEED = 900.0

@onready var _timer = $Timer

const projectile = preload("res://Scenes/Weapons/projectiles/projectile_wand.tscn")

func _ready():
	_timer.wait_time = INTERVAL
	
func _process(_delta):
	transform = transform.looking_at(get_parent().velocity)

func _on_timer_timeout():
	var player_ref = get_tree().get_first_node_in_group("Player")
	var enemies = get_tree().get_nodes_in_group("EnemyBody")
	if len(enemies) == 0:
		return
	
	var new_projectile = projectile.instantiate()
	get_parent().get_parent().add_child(new_projectile)
	new_projectile.transform = get_global_transform()
	
	# TODO(Lena): Optimize this by only getting close enemy bodies
	var closest_enemy_distance = 10000000.0
	var closest_enemy = null
	for enemy in enemies:
		var distance = enemy.global_position.distance_to(player_ref.transform.origin)
		if distance < closest_enemy_distance:
			closest_enemy = enemy
			closest_enemy_distance = distance
	
	var angle = player_ref.transform.looking_at(closest_enemy.global_position)
	
	new_projectile.velocity = Vector2.from_angle(angle.get_rotation()) * PROJECTILE_SPEED
	
	new_projectile.DAMAGE = DAMAGE
	_timer.start()
