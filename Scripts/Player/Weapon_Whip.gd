extends Node
class_name Weapon_Whip

@export var INTERVAL = 1.0
@export var ACTIVE_TIME = 0.1
@export var DAMAGE = 1
@export var SAFE_AREA_LIMIT = 100.0

var animationStep = 5
var animationCounter = 0
const ANIMATION_SPEED = 0.02

var animationFrame1 = preload("res://Assets/Attack_Animations/twirl_01.png")
var animationFrame2 = preload("res://Assets/Attack_Animations/twirl_02.png")
var animationFrame3 = preload("res://Assets/Attack_Animations/twirl_03.png")

var animationFrames = [animationFrame1, animationFrame1, animationFrame2, animationFrame3, animationFrame3]

@onready var _timer = $Timer
@onready var _damageArea = $WhipDamageArea
@onready var _animation = $WhipDamageArea/Animation

func _ready():
	_timer.wait_time = INTERVAL
	_animation.visible = true
	
func _process(delta):
	_animation.rotate(-0.2)
	
	if animationStep >= len(animationFrames):
		return
	animationCounter += delta
	if animationCounter < ANIMATION_SPEED:
		return
	animationStep += 1
	if animationStep == len(animationFrames):
		_animation.texture = null
	else:
		_animation.texture = animationFrames[animationStep]
	animationCounter = 0
	

func _on_timer_timeout():
	if _damageArea.visible:
		_damageArea.monitoring = false
		_damageArea.visible = false
		_timer.wait_time = INTERVAL
	else:
		_damageArea.monitoring = true	
		_damageArea.visible = true
		_timer.wait_time = ACTIVE_TIME
		animationStep = 0
		animationCounter = ANIMATION_SPEED
	_timer.start()

func _on_whip_damage_area_entered(area):
	if area.is_in_group("Enemy") or area.is_in_group("Projectile"):
		var transform = get_tree().get_first_node_in_group("Player").transform
		if area.get_parent().transform.get_origin().distance_to(transform.get_origin()) > SAFE_AREA_LIMIT:
			if area.is_in_group("Enemy"):
				area.get_parent().damageIfHit(DAMAGE)
			if area.is_in_group("Projectile"):
				if area.get_parent().ProjectileType == GlobalConstants.ProjectileType.BREAKABLE:
					area.get_parent().queue_free()
