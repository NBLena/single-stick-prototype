extends Node2D
class_name Weapon_Sword

@export var INTERVAL = 1.0
@export var ACTIVE_TIME = 0.1
@export var DAMAGE = 1

@onready var _timer = $Timer
@onready var _damageArea = $SwordDamageArea

func _ready():
	_timer.wait_time = INTERVAL
	
func _process(_delta):
	transform = transform.looking_at(get_parent().velocity)

func _on_timer_timeout():
	if _damageArea.visible:
		_damageArea.monitoring = false
		_damageArea.visible = false
		_timer.wait_time = INTERVAL
	else:
		_damageArea.monitoring = true	
		_damageArea.visible = true
		_timer.wait_time = ACTIVE_TIME
	_timer.start()

func _on_sword_damage_area_area_entered(area):
	if area.is_in_group("Enemy"):
		area.get_parent().damageIfHit(DAMAGE)
	if area.is_in_group("Projectile"):
		if area.get_parent().ProjectileType == GlobalConstants.ProjectileType.BREAKABLE:
			area.get_parent().queue_free()
