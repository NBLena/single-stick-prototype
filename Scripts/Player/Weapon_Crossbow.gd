extends Node2D
class_name Weapon_Crossbow

@export var INTERVAL = 1.0
@export var DAMAGE = 1.0
@export var PROJECTILE_SPEED = 900.0

@onready var _timer = $Timer

const projectile = preload("res://Scenes/Weapons/projectiles/projectile_crossbow.tscn")

func _ready():
	_timer.wait_time = INTERVAL
	
func _process(_delta):
	transform = transform.looking_at(get_parent().velocity)

func _on_timer_timeout():
	var new_projectile = projectile.instantiate()
	get_parent().get_parent().add_child(new_projectile)
	new_projectile.transform = get_global_transform()
	new_projectile.velocity = Vector2.from_angle(transform.get_rotation()) * PROJECTILE_SPEED
	new_projectile.DAMAGE = DAMAGE
	_timer.start()
