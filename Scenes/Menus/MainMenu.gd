extends Node2D

enum {START, LEVEL_SELECT, OPTIONS, QUIT}
var NUM_ITEMS = 3
var selected_item:int = START

# Called when the node enters the scene tree for the first time.
func _ready():
	$"Buttons/Start Game/AnimationPlayer".play("selected")


func _input(event):
	if event.is_action_pressed("Move_Down"):
		play_anim("not_selected")
		selected_item += 1
		if selected_item > NUM_ITEMS:
			selected_item = 0
		# Skip Level Select, for now, as it is not implemented
		if selected_item == LEVEL_SELECT:
			selected_item += 1
		play_anim("selected")
	if event.is_action_pressed("Move_Up"):
		play_anim("not_selected")
		selected_item -= 1
		if selected_item < 0:
			selected_item = NUM_ITEMS
		# Skip Level Select, for now, as it is not implemented
		if selected_item == LEVEL_SELECT:
			selected_item -= 1
		play_anim("selected")
	if event.is_action_pressed("Move_Right"):
		play_anim("confirmed")
	if event.is_action_pressed("QuitGame"):
		get_tree().quit()

func play_anim(animation):
	$Buttons.get_child(selected_item).find_child("AnimationPlayer").play(animation)


func _on_animation_player_animation_finished(anim_name):
	if anim_name == "confirmed":
		if selected_item == QUIT:
			get_tree().quit()
		elif selected_item == START:
			get_tree().change_scene_to_file("res://Scenes/Menus/Intro_Cutscene.tscn")
		elif selected_item == OPTIONS:
			get_tree().change_scene_to_file("res://Scenes/Menus/OptionsMenu.tscn")
		else:
			# Not implemted yet, restart main menu scene for now
			get_tree().reload_current_scene()
