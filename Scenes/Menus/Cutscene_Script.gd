extends Node2D
@export var NEXT_LEVEL: PackedScene

func _input(event):
	if event.is_action_pressed("ui_accept"):
		move_on_to_first_level()

func move_on_to_first_level():
	if not NEXT_LEVEL or NEXT_LEVEL == null:
		push_error("No next Scene/Level was set in Cutscene script!")
	if get_tree().change_scene_to_file(NEXT_LEVEL.resource_path) == ERR_CANT_OPEN:
		push_error("Cutscne Script was not given a valid Level/Scene to load next.")
