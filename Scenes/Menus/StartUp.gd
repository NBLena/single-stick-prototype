extends Node2D

func _ready():
	$Container/AnimationPlayer.play("startup_animation")

func _input(event):
	if event.is_action_pressed("ui_accept"):
		move_to_main_menu()

func move_to_main_menu():
	get_tree().change_scene_to_file("res://Scenes/Menus/MainMenu.tscn")
