extends Node2D

enum {VOLUME, FULLSCREEN, DASH_BY_FLICK, DASH_BY_DOUBLE_TAP, DASH_BY_BUTTON}
var NUM_ITEMS = 4
var selected_item:int = VOLUME

func _ready():
	updateOptionsValues()

func updateOptionsValues():
	$Container/Options/Fullscreen/Value.text = "Enabled" if get_window().mode == get_window().MODE_FULLSCREEN else "Disabled"
	
	$Container/Options/DashByFlick/Value.text = "Enabled" if UserOptions.dashByFlick else "Disabled"
	$Container/Options/DashByDoubleTap/Value.text = "Enabled" if UserOptions.dashByDoubleTap else "Disabled"
	$Container/Options/DashByButton/Value.text = "Enabled" if UserOptions.dashByButton else "Disabled"

func changeOptionValue():
	if selected_item == DASH_BY_FLICK:
		UserOptions.dashByFlick = !UserOptions.dashByFlick
	elif selected_item == DASH_BY_DOUBLE_TAP:
		UserOptions.dashByDoubleTap = !UserOptions.dashByDoubleTap
	elif selected_item == DASH_BY_BUTTON:
		UserOptions.dashByButton = !UserOptions.dashByButton
	elif selected_item == FULLSCREEN:
		get_window().set_mode(Window.MODE_FULLSCREEN if get_window().mode == get_window().MODE_WINDOWED else Window.MODE_WINDOWED)
		if get_window().mode == get_window().MODE_WINDOWED:
			get_window().size = Vector2i(1280, 720)
	updateOptionsValues()

func _input(event):
	# Leave options Menu
	if event.is_action_pressed("Move_Left"):
		$AnimationPlayer.play("Transition_Out")
	
	# Menu Stuff
	if event.is_action_pressed("Move_Down"):
		play_anim("Options_Unselected")
		selected_item += 1
		if selected_item > NUM_ITEMS:
			selected_item = 0
		play_anim("Options_Selected")
	if event.is_action_pressed("Move_Up"):
		play_anim("Options_Unselected")
		selected_item -= 1
		if selected_item < 0:
			selected_item = NUM_ITEMS
		play_anim("Options_Selected")
	if event.is_action_pressed("Move_Right"):
		changeOptionValue()
	if event.is_action_pressed("QuitGame"):
		$AnimationPlayer.play("Transition_Out")
	

func play_anim(animation):
	$Container/Options.get_child(selected_item).find_child("AnimationPlayer").play(animation)

func return_to_main_menu_scene():
	get_tree().change_scene_to_file("res://Scenes/Menus/MainMenu.tscn")
